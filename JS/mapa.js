console.log("Hello Juli");
// https://cesium.com/learn/cesiumjs-learn/cesiumjs-quickstart/

// Set Token
Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0Y2E4YjcyNS1jNjM0LTQ1OGEtYmM3MS00NzQxOTFiYmFlMjgiLCJpZCI6MTM0NzUzLCJpYXQiOjE2ODIwMDk5NDN9.GnlQ9kjF0KfXvNQWkNtZObCUZYo2vpHqIw22r_d7v2E';

 // Initialize the Cesium Viewer in the HTML element with the `cesiumContainer` ID.
 const viewer = new Cesium.Viewer('cesiumContainer', {
    terrainProvider: Cesium.createWorldTerrain()
  });

// Add Cesium OSM Buildings, a global 3D buildings layer.
const buildingTileset = viewer.scene.primitives.add(Cesium.createOsmBuildings());   
// Fly the camera to San Francisco at the given longitude, latitude, and height.
viewer.camera.flyTo({
  destination : Cesium.Cartesian3.fromDegrees(-74.18464991886866, 4.610158295511055, 300),
  orientation : {
    heading : Cesium.Math.toRadians(0.0),
    pitch : Cesium.Math.toRadians(15.0),
  }
});
